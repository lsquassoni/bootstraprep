#include "ComplexSystemApp.h"

int main() {
	
	auto app = new ComplexSystemApp();
	app->run("AIE", 1280, 720, false);
	delete app;

	return 0;
}