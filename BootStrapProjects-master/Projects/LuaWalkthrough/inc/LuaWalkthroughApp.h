#pragma once

#include "Application.h"
#include <glm/mat4x4.hpp>
#include "sol.hpp"

class Entity
{
public:
	Entity();
	~Entity();

	void Update();

	void Draw();

	//Getters
	float GetX() { return x; }
	float GetY() { return y; }
	float GetZ() { return z; }
	//Setters
	void SetX(float a_x) { x = a_x; }
	void SetY(float a_y) { y = a_y; }
	void SetZ(float a_z) { z = a_z; }
	
private:
	float x = 0;
	float y = 0;
	float z = 0;
};

class LuaWalkthroughApp : public aie::Application {
public:

	LuaWalkthroughApp();
	virtual ~LuaWalkthroughApp();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

protected:

	sol::state m_lua;

	// camera transforms
	glm::mat4	m_viewMatrix;
	glm::mat4	m_projectionMatrix;
};