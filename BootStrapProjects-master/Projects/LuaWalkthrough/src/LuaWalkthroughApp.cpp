#include "LuaWalkthroughApp.h"
#include "Gizmos.h"
#include "Input.h"
#include <glm/glm.hpp>
#include <glm/ext.hpp>

using glm::vec3;
using glm::vec4;
using glm::mat4;
using aie::Gizmos;

LuaWalkthroughApp::LuaWalkthroughApp() {

}

LuaWalkthroughApp::~LuaWalkthroughApp() {

}

bool LuaWalkthroughApp::startup() {
	
	setBackgroundColour(0.25f, 0.25f, 0.25f);

	// initialise gizmo primitive counts
	Gizmos::create(10000, 10000, 10000, 10000);

	// create simple camera transforms
	m_viewMatrix = glm::lookAt(vec3(10), vec3(0), vec3(0, 1, 0));
	m_projectionMatrix = glm::perspective(glm::pi<float>() * 0.25f, 16.0f / 9.0f, 0.1f, 1000.0f);

	m_lua.open_libraries(sol::lib::base);
	m_lua.new_usertype<Entity>(
		"Entity", sol::constructors<Entity()>(),
		"Update", &Entity::Update,
		"Draw",	&Entity::Draw,
		"xPos", sol::property(&Entity::GetX, &Entity::SetX),
		"yPos", sol::property(&Entity::GetY, &Entity::SetY),
		"zPos", sol::property(&Entity::GetZ, &Entity::SetZ)
		);
	m_lua.script_file("./scripts/HelloWorld.lua");
	return true;
}

void LuaWalkthroughApp::shutdown() {

	Gizmos::destroy();
}

void LuaWalkthroughApp::update(float deltaTime) {


	// wipe the gizmos clean for this frame
	Gizmos::clear();
	m_lua["Update"]();
	// draw a simple grid with gizmos
	vec4 white(1);
	vec4 black(0, 0, 0, 1);
	for (int i = 0; i < 21; ++i) {
		Gizmos::addLine(vec3(-10 + i, 0, 10),
						vec3(-10 + i, 0, -10),
						i == 10 ? white : black);
		Gizmos::addLine(vec3(10, 0, -10 + i),
						vec3(-10, 0, -10 + i),
						i == 10 ? white : black);
	}

	// add a transform so that we can see the axis
	Gizmos::addTransform(mat4(1));

	// quit if we press escape
	aie::Input* input = aie::Input::getInstance();

	if (input->wasKeyPressed(aie::INPUT_KEY_F5))
	{
		m_lua.script_file("./scripts/HelloWorld.lua");
	}

	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();
}

void LuaWalkthroughApp::draw() {

	// wipe the screen to the background colour
	clearScreen();
	m_lua["Draw"]();
	// update perspective based on screen size
	m_projectionMatrix = glm::perspective(glm::pi<float>() * 0.25f, getWindowWidth() / (float)getWindowHeight(), 0.1f, 1000.0f);

	Gizmos::draw(m_projectionMatrix * m_viewMatrix);
}

Entity::Entity()
{
}

Entity::~Entity()
{
}

void Entity::Update()
{
}

void Entity::Draw()
{
	Gizmos::addSphere(glm::vec3(x, y, z), 1.0f, 16, 16, glm::vec4(1, 1, 1, 1));
}
