#include "LuaWalkthroughApp.h"
#include "lua.hpp"

int main() {
	
	auto app = new LuaWalkthroughApp();
	app->run("AIE", 1280, 720, false);
	delete app;

	return 0;
}