#include "CubeChallengeApp.h"

int main() {
	
	auto app = new CubeChallengeApp();
	app->run("AIE", 1280, 720, false);
	delete app;

	return 0;
}