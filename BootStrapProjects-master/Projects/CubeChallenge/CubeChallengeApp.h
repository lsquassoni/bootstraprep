#pragma once

#include "Application.h"
#include <glm/mat4x4.hpp>
#include <glm/vec2.hpp>
#include <Texture.h>
#include <vector>

using aie::Texture;

class CubeChallengeApp : public aie::Application {
public:

	CubeChallengeApp();
	virtual ~CubeChallengeApp();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

	void DrawGrid();
	
	void LoadShader();
	void UnloadShader();

	void CreateCube();
	void DestroyCube();

	void LoadTexture(const char* a_fileName);
	void UnloadTextures();


protected:

	glm::mat4	m_viewMatrix;
	glm::mat4	m_projectionMatrix;

	unsigned int m_shader;

	Texture* m_tTexture = new aie::Texture();
	std::vector<Texture*> m_vTextures;

	unsigned int m_cubeIndicesCount;

	unsigned int m_cubeVao;
	unsigned int m_cubeVbo;
	unsigned int m_cubeIbo;

	struct Vertex
	{

		glm::vec4 pos;
		glm::vec4 col;
		glm::vec2 uv;

		static void SetupVertexAttribPointers();

	};

};