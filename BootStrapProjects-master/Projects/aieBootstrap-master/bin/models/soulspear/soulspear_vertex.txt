#version 430
layout (location = 0)in vec4 position;
layout (location = 1)in vec3 normal;
layout (location = 2)in vec2 uv;
out vec3 vPosition;
out vec3 vNormal;
out vec2 vuv;
uniform mat4 projectionViewWorldMatrix;
void main() 
{
	vNormal = normal;
	vPosition = position.xyz;
	vuv = uv;
	gl_Position = projectionViewWorldMatrix * position;
}