#version 430
layout (location = 0)in vec4 position;
layout (location = 1)in vec4 normal;
layout (location = 2)in vec4 tangent;
layout (location = 3)in vec2 texcoord;
layout (location = 4)in vec4 weights;
layout (location = 5)in vec4 indices;

out vec3 frag_normal;
out vec3 frag_position;
out vec3 frag_tangent;
out vec3 frag_bitangent;
out vec2 frag_texcoord;

uniform mat4 projectionView;
//uniform mat4 global;

const int MAX_BONES = 128;
uniform mat4 bones[MAX_BONES];

void main() 
{
	frag_position = position.xyz;
	frag_normal = normal.xyz;
	frag_tangent = tangent.xyz;
	frag_bitangent = cross(normal.xyz, tangent.xyz);
	frag_texcoord = texcoord;
	
	ivec4 index = ivec4(indices);
	
	vec4 p = bones[index.x] * position * weights.x;
	p += bones[index.y] * position * weights.y;
	p += bones[index.z] * position * weights.z;
	p += bones[index.w] * position * weights.w;
	gl_Position = projectionView * p;
}