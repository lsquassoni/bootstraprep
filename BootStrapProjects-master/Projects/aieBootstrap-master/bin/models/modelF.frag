#version 430
in vec3 frag_normal;
in vec3 frag_position;
in vec2 frag_texcoord;
out vec4 FragColor;
uniform sampler2D diffuseTexture;
uniform float lightAmbientStrength;
uniform float lightStrength;
uniform vec3 lightPosition;
uniform vec3 lightColor;
uniform vec3 specularColor;
uniform vec3 cameraPosition;

vec3 CalcSpecularLighting(vec3 norm, vec3 lightDir);
vec3 CalcDiffColor(vec3 norm, vec3 lightDir);

void main ()
{
	vec3 norm = normalize(frag_normal.xyz);
	vec3 lightDir = normalize(lightPosition - frag_position);

	vec3 specColor = CalcSpecularLighting(norm, lightDir);
	vec3 diffColor = CalcDiffColor(norm, lightDir);
	
	vec4 texColor = texture2D(diffuseTexture, frag_texcoord);
	//vec4 texColor = vec4(norm, 1.0f);
	vec3 ambient = texColor.rgb * lightColor * lightAmbientStrength;
	
	FragColor = texColor * vec4(ambient + diffColor + specColor, 1.0f);
}

vec3 CalcSpecularLighting(vec3 norm, vec3 lightDir)
{
	vec3 viewDir = normalize(cameraPosition - frag_position);	
	vec3 reflectDir = reflect(-lightDir, norm);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0f), 50.0f);
	
	return (spec * specularColor);
}
vec3 CalcDiffColor(vec3 norm, vec3 lightDir)
{
	float diff = max(dot(norm, lightDir), 0.0f);
	return (diff * lightColor * lightStrength);
}