#version 430
layout (location = 0)in vec4 vPosition;
layout (location = 1)in vec2 vUV;
layout (location = 2)in vec3 vNorm;
out vec3 fPos;
out vec3 fNorm;
out vec2 fUV;
uniform mat4 projectionView;
void main()
{
fNorm = vNorm;
fPos = vPosition.xyz;
fUV = vUV;
gl_Position = projectionView * vPosition;
}