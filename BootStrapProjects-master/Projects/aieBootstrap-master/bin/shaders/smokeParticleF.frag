//smokeParticleF
#version 430

in vec4 fcolor;
in vec2 ftexCoord;

out vec4 frag_color;

uniform sampler2D frag_texture;

vec4 GetTextureColor()
{
	vec4 texColor = texture2D(frag_texture, ftexCoord);
	return texColor;
}

void main()
{
	frag_color = GetTextureColor() * fcolor;
}