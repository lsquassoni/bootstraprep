#version 430
layout (location = 0) in vec4 vPosition;
layout (location = 1) in vec2 vTexCoords;
layout (location = 2) in vec3 vNormal;

out vec3 fPosition;
out vec3 fNormal;
out vec2 fTexCoords;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

void main()
{
	gl_Position = projection * view * model * vec4(vPosition, 1.0f);
	fPosition = vec3(model * vec4(vPosition, 1.0f);
	fNormal = mat3(transpose(inverse(model))) * vNormal;
	fTexCoords = vTexCoords;
}