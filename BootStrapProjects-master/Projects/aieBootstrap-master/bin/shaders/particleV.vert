#version 430

layout (location = 0) in vec4 position; 
layout (location = 1) in vec4 color;
layout (location = 2) in vec2 texCoord;

out vec4 fcolor;
out vec2 ftexCoord;

uniform mat4 projectionView;

void main()
{
	fcolor = color;
	ftexCoord = texCoord;
	gl_Position = projectionView * position;
}