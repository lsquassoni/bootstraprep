#version 430
in vec2 fUV;
in vec3 fNorm;
in vec3 fPos;
out vec4 frag_color;
uniform sampler2D tileTexture;
uniform sampler2D splatMap;
uniform sampler2D grass;
uniform sampler2D snow;
uniform float lightAmbientStrength;
uniform float lightStrength;
uniform vec3 lightPosition;
uniform vec3 lightColor;
uniform vec3 specularColor;
uniform vec3 cameraPosition;

vec3 CalcSpecularLighting(vec3 norm, vec3 lightDir);
vec3 CalcDiffColor(vec3 norm, vec3 lightDir);
vec4 CalcSplatTexturing();

void main ()
{
	vec3 norm = normalize(fNorm.xyz);
	vec3 lightDir = normalize(lightPosition - fPos);

	vec3 specColor = CalcSpecularLighting(norm, lightDir);
	vec3 diffColor = CalcDiffColor(norm, lightDir);
	
	vec4 texColor = CalcSplatTexturing();
	vec3 ambient = texColor.rgb * lightColor * lightAmbientStrength;
	
	texColor.a = 1.0f;
	frag_color = texColor * vec4(ambient + diffColor + specColor, 1.0f);
}

vec3 CalcSpecularLighting(vec3 norm, vec3 lightDir)
{
	vec3 viewDir = normalize(cameraPosition - fPos);	
	vec3 reflectDir = reflect(-lightDir, norm);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0f), 10000.0f);
	
	return (spec * specularColor);
}
vec3 CalcDiffColor(vec3 norm, vec3 lightDir)
{
	float diff = max(dot(norm, lightDir), 0.0f);
	return (diff * lightColor * lightStrength);
}
vec4 CalcSplatTexturing()
{
	vec4 splatColor = texture2D(splatMap, fUV);
	return (splatColor.r * texture2D(snow, fUV) + splatColor.g * texture2D(grass, fUV) + splatColor.b * vec4(0.0f, 0.2f, 0.8f, 1.0f));
}