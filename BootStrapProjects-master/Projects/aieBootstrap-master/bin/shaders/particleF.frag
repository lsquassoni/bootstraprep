#version 430

in vec4 fcolor;
in vec2 ftexCoord;
out vec4 fragColor;

void main()
{
	fragColor = fcolor;
}