#ifndef _CAMERA_H_
#define _CAMERA_H_

#include <glm\vec3.hpp>
#include <glm\mat4x4.hpp>
#include <Input.h>

using glm::vec3;
using glm::mat4;


class Camera
{
public:
	Camera();
	~Camera();

	void Update(float deltaTime);

	void LookAt(vec3 target);
	void SetPosition(vec3 position);

	void CameraInput(aie::Input *input, float deltaTime);

	void GetFrustumPlanes(const glm::mat4& transform, glm::vec4* planes);

	const glm::vec3 GetPosition() { return m_vPosition; }
	float GetMouseSensitivity() { return m_fMouseSensitivity; }

	const glm::mat4 & GetView();



private:
	void CalculateLook(); /*Gives us the vector of direction using Yaw, Pitch and Roll*/
	void CalculateView();/* sets up the view Matrix based on our camera information*/


	glm::mat4 m_mViewMatrix;/*The matrix we'll pass to Open GL so it 
					   knows how the camera looks at the world*/

	vec3 m_vCameraLook; /*the actual direction the camera is aiming*/

	vec3 m_vCameraUp = vec3(0.0f, 1.0f, 0.0f); /*Setting camera orientation 
											   so the y axis is always up*/

	vec3 m_vPosition;
	float m_fYaw = 0.0f;   /* Rotate on Y Axis */
	float m_fPitch = 0.0f; /* Rotate on X Axis */
	float m_fRoll = 0.0f;  /* Rotate on Z Axis */

	const float m_fMouseSensitivity = 10.0f;
	int m_iLastMouseXPos = 0;	/*Recording previous mouse position */
	int m_iLastMouseYPos = 0;	/*so we can see how far it has moved*/
	const float m_fMoveSpeed = 5.0f;

};

#endif
