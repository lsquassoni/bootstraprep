#include "Particles.h"
#include <gl_core_4_4.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include "Camera.h"
#include "../Shader.h"

ParticleEmitter::ParticleEmitter()
	:m_particles(nullptr),
	m_firstDead(0),
	m_maxParticles(0),
	m_position(0, 0, 0),
	m_vao(0), m_vbo(0), m_ibo(0),
	m_vertexData(nullptr),
	m_texture(nullptr) {

}


ParticleEmitter::~ParticleEmitter()
{
	delete[] m_particles;
	delete[] m_vertexData;

	glDeleteVertexArrays(1, &m_vao);
	glDeleteBuffers(1, &m_vbo);
	glDeleteBuffers(1, &m_ibo);

}

void ParticleEmitter::initialize(unsigned int a_maxParticles, 
	unsigned int a_emitRate, float a_lifetimeMin, float a_lifetimeMax, 
	float a_velocityMin, float a_velocityMax, float a_startSize, 
	float a_endSize, const vec4 & a_startColor, const vec4 & a_endColor, vec3& a_direction)
{
	//set up emit timers
	m_emitTimer = 0;
	m_emitRate = 1.0f / a_emitRate;

	for (unsigned int i = 0; i < a_maxParticles; i++)
	{
		//fill vector with particles with new memory allocated
		Particle* temp = new Particle();
		m_currentParticles.push_back(temp);
	}

	//store all variables passed in
	m_startColor = a_startColor;
	m_endColor = a_endColor;
	m_startSize = a_startSize;
	m_endSize = a_endSize;
	m_velocityMin = a_velocityMin;
	m_velocityMax = a_velocityMax;
	m_lifespanMin = a_lifetimeMin;
	m_lifespanMax = a_lifetimeMax;
	m_maxParticles = a_maxParticles;
	m_direction = a_direction;

	//create particle array
	m_particles = new Particle[m_maxParticles];
	m_firstDead = 0;

	//create the array vertices for the particles
	//4 vertices per particle for a quad.
	//will be filled during update
	m_vertexData = new ParticleVertex[m_maxParticles * 4];

	//create the index buffer data for the particles
	//6 indices per quad of 2 triangles
	//fill it now as it never changes
	unsigned int * indexData = new unsigned int[m_maxParticles * 6];
	for (unsigned int i = 0; i < m_maxParticles; ++i)
	{
		indexData[i * 6 + 0] = i * 4 + 0;
		indexData[i * 6 + 1] = i * 4 + 1;
		indexData[i * 6 + 2] = i * 4 + 2;

		indexData[i * 6 + 3] = i * 4 + 0;
		indexData[i * 6 + 4] = i * 4 + 2;
		indexData[i * 6 + 5] = i * 4 + 3;
	}

	//create openGL buffers
	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

	glGenBuffers(1, &m_vbo);
	glGenBuffers(1, &m_ibo);

	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, m_maxParticles * 4 * sizeof(ParticleVertex), m_vertexData, GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_maxParticles * 6 * sizeof(unsigned int), indexData, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(ParticleVertex), 0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(ParticleVertex), ((char*)0) + 16);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(ParticleVertex), ((char*)0) + 32);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	delete[] indexData;
}

void ParticleEmitter::SetTexture(aie::Texture * a_texture)
{
	m_texture = a_texture;
}

void ParticleEmitter::emit()
{
	//only emit if there is a dead particle to use
	if (m_firstDead >= m_maxParticles)
		return;

	//resurrect the first dead particle
	Particle& particle = m_particles[m_firstDead++];
	
	//assign its starting position
	particle.m_position = m_position;

	//randomise its lifespan
	particle.m_lifetime = 0;
	particle.m_lifespan = (rand() / (float)RAND_MAX) * (m_lifespanMax - m_lifespanMin) + m_lifespanMin;

	//set starting size and color
	particle.m_color = m_startColor;
	particle.m_size = m_startSize;
	
	//randomise velocity direction and strength
	float velocity = (rand() / (float)RAND_MAX)*(m_velocityMax - m_velocityMin) + m_velocityMin;
	particle.m_velocity.x = (rand() / (float)RAND_MAX) * 2 - 1;
	particle.m_velocity.y = (rand() / (float)RAND_MAX) * 2 - 1;
	particle.m_velocity.z = (rand() / (float)RAND_MAX) * 2 - 1;
	particle.m_velocity = glm::normalize(particle.m_velocity) * velocity;
}

void ParticleEmitter::update(float a_deltaTime, Camera* a_camera, vec3 a_emitterPosition)
{
	//spawn particles
	m_emitTimer += a_deltaTime;
	while (m_emitTimer > m_emitRate)
	{
		emit();
		m_emitTimer -= m_emitRate;
	}
	m_position = a_emitterPosition;
	unsigned int quad = 0;

	//update particles and turn live particles into billboard quads
	for (unsigned int i = 0; i < m_firstDead; i++)
	{
		Particle* particle = &m_particles[i];

		m_currentParticles.at(i) = particle; //assign Particle at index i the information of the particle variable

		particle->m_lifetime += a_deltaTime;
		if (particle->m_lifetime >= particle->m_lifespan)
		{
			//swap last alive with this one
			*particle = m_particles[m_firstDead - 1];
			m_firstDead--;			
		}
		else
		{
			//move particle
			particle->m_velocity += m_direction;
			particle->m_position += particle->m_velocity * a_deltaTime;
			//size particle
			particle->m_size = glm::mix(m_startSize, m_endSize, particle->m_lifetime / particle->m_lifespan);

			//color particle
			particle->m_color = glm::mix(m_startColor, m_endColor, particle->m_lifetime / particle->m_lifespan);

			//make a quad the correct size and color
			float halfSize = particle->m_size * 0.5f;

			m_vertexData[quad * 4 + 0].position = vec4(halfSize, halfSize, 0, 1); 
			m_vertexData[quad * 4 + 0].colour = particle->m_color; 
			
			m_vertexData[quad * 4 + 1].position = vec4(-halfSize, halfSize, 0, 1);
			m_vertexData[quad * 4 + 1].colour = particle->m_color;
			
			m_vertexData[quad * 4 + 2].position = vec4(-halfSize, -halfSize, 0, 1); 
			m_vertexData[quad * 4 + 2].colour = particle->m_color; 
			
			m_vertexData[quad * 4 + 3].position = vec4(halfSize, -halfSize, 0, 1);
			m_vertexData[quad * 4 + 3].colour = particle->m_color;
			

			if (m_texture != nullptr)
			{
				m_vertexData[quad * 4 + 0].texCoord = vec2(0, 0);
				m_vertexData[quad * 4 + 1].texCoord = vec2(1, 0);
				m_vertexData[quad * 4 + 2].texCoord = vec2(1, 1);
				m_vertexData[quad * 4 + 3].texCoord = vec2(0, 1);
			}

			//create billboard transform
			vec3 zAxis = glm::normalize(a_camera->GetPosition() - particle->m_position);
			vec3 xAxis = glm::cross(vec3(a_camera->GetView()[1]), zAxis);
			vec3 yAxis = glm::cross(zAxis, xAxis);
			mat4 billboard(	vec4(xAxis, 0),
							vec4(yAxis, 0),
							vec4(zAxis, 0),
							vec4(0, 0, 0, 1));

			m_vertexData[quad * 4 + 0].position = billboard * m_vertexData[quad * 4 + 0].position + vec4(particle->m_position, 0);
			m_vertexData[quad * 4 + 1].position = billboard * m_vertexData[quad * 4 + 1].position + vec4(particle->m_position, 0);
			m_vertexData[quad * 4 + 2].position = billboard * m_vertexData[quad * 4 + 2].position + vec4(particle->m_position, 0);
			m_vertexData[quad * 4 + 3].position = billboard * m_vertexData[quad * 4 + 3].position + vec4(particle->m_position, 0);

			++quad;
		}
	}

}

void ParticleEmitter::draw(mat4 a_projection)
{
	glUseProgram(m_particleShader);
	int loc = glGetUniformLocation(m_particleShader, "projectionView");
	glUniformMatrix4fv(loc, 1, GL_FALSE, &a_projection[0][0]);

	if (m_texture != nullptr)
	{
		glActiveTexture(GL_TEXTURE0);
		/*Bind texture*/
		glBindTexture(GL_TEXTURE_2D, m_texture->getHandle());
		glUniform1i(glGetUniformLocation(m_particleShader, "frag_texture"), 0);
	}

	//sync the particle vertex buffer
	//based on how many alive particles there are
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferSubData(GL_ARRAY_BUFFER, 0, m_firstDead * 4 * sizeof(ParticleVertex), m_vertexData);

	//draw particles
	glBindVertexArray(m_vao);
	glDrawElements(GL_TRIANGLES, m_firstDead * 6, GL_UNSIGNED_INT, 0);

	glUseProgram(0);
}

void ParticleEmitter::loadShader(const char* a_vs, const char* a_fs)
{
	static const char* vertex_shader = a_vs;

	static const char* fragment_shader = a_fs;

	// Step 1:
	// Load the vertex shader, provide it with the source code and compile it.
	GLuint vs = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs, 1, &vertex_shader, NULL);
	glCompileShader(vs);

	// Step 2:
	// Load the fragment shader, provide it with the source code and compile it.
	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, &fragment_shader, NULL);
	glCompileShader(fs);

	// step 3:
	// Create the shader program
	m_particleShader = glCreateProgram();

	// Step 4:
	// attach the vertex and fragment shaders to the m_shader program
	glAttachShader(m_particleShader, vs);
	glAttachShader(m_particleShader, fs);

	// Step 5:
	// describe the location of the shader inputs the link the program
	glLinkProgram(m_particleShader);

	// step 6:
	// delete the vs and fs shaders
	glDeleteShader(vs);
	glDeleteShader(fs);

}


GPUParticleEmitter::GPUParticleEmitter() :
	m_particles(nullptr), m_maxParticles(0),
	m_position(0, 0, 0),
	m_drawShader(0),
	m_updateShader(0)
{
		m_vao[0] = 0;
		m_vao[1] = 0;
		m_vbo[0] = 0;
		m_vbo[1] = 0;
}

GPUParticleEmitter::~GPUParticleEmitter()
{
	delete[] m_particles;

	glDeleteVertexArrays(2, m_vao);
	glDeleteBuffers(2, m_vbo);

	glDeleteProgram(m_drawShader);
	glDeleteProgram(m_updateShader);
}

void GPUParticleEmitter::Initialise(unsigned int a_maxParticles,
	float a_lifetimeMin, float a_lifetimeMax,
	float a_velocityMin, float a_velocityMax,
	float a_startSize, float a_endSize,
	const vec4& a_startColor,
	const vec4& a_endColor)
{
	m_startColor = a_startColor;
	m_endColor = a_endColor;
	m_startSize = a_startSize;
	m_endSize = a_endSize;
	m_velocityMin = a_velocityMin;
	m_velocityMax = a_velocityMax;
	m_lifespanMin = a_lifetimeMin;
	m_lifespanMax = a_lifetimeMax;
	m_maxParticles = a_maxParticles;

	m_particles = new GPUParticle[a_maxParticles];

	m_activeBuffer = 0;

	CreateBuffers();
	CreateUpdateShader();
	CreateDrawShader();
}

void GPUParticleEmitter::Draw(float time, const mat4& a_cameraTransform, const mat4& a_projectionView)
{
	glUseProgram(m_updateShader);

	int location = glGetUniformLocation(m_updateShader, "time");
	glUniform1f(location, time);

	float deltaTime = time - m_lastDrawTime; m_lastDrawTime = time;

	location = glGetUniformLocation(m_updateShader, "deltaTime");
	glUniform1f(location, deltaTime);

	location = glGetUniformLocation(m_updateShader, "emitterPosition");
	glUniform3fv(location, 1, &m_position[0]);

	glEnable(GL_RASTERIZER_DISCARD);

	glBindVertexArray(m_vao[m_activeBuffer]);

	unsigned int otherbuffer = (m_activeBuffer + 1) % 2;

	glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, m_vbo[otherbuffer]);
	glBeginTransformFeedback(GL_POINTS);

	glDrawArrays(GL_POINTS, 0, m_maxParticles);

	glEndTransformFeedback();
	glDisable(GL_RASTERIZER_DISCARD);
	glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);

	glUseProgram(m_drawShader);

	location = glGetUniformLocation(m_drawShader, "projectionView");
	glUniformMatrix4fv(location, 1, false, &a_projectionView[0][0]);

	location = glGetUniformLocation(m_drawShader, "cameraTransform");
	glUniformMatrix4fv(location, 1, false, &a_cameraTransform[0][0]);

	glBindVertexArray(m_vao[otherbuffer]);
	glDrawArrays(GL_POINTS, 0, m_maxParticles);

	m_activeBuffer = otherbuffer;
}

void GPUParticleEmitter::CreateBuffers()
{
	glGenVertexArrays(2, m_vao);
	glGenBuffers(2, m_vbo);

	glBindVertexArray(m_vao[0]);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, m_maxParticles * sizeof(GPUParticle), m_particles, GL_STREAM_DRAW);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), 0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), ((char*)0) + 12);
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), ((char*)0) + 24);
	glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), ((char*)0) + 28);

	glBindVertexArray(m_vao[1]);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, m_maxParticles * sizeof(GPUParticle), 0, GL_STREAM_DRAW);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), 0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), ((char*)0) + 12);
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), ((char*)0) + 24);
	glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, sizeof(GPUParticle), ((char*)0) + 28);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void GPUParticleEmitter::CreateUpdateShader()
{
	Shader updateShader("shaders/GPUparticleUpdate.txt");
	static const char* vertex_shader = updateShader.m_vertex.c_str();
	GLuint vs = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs, 1, &vertex_shader, NULL);
	glCompileShader(vs);

	m_updateShader = glCreateProgram();
	glAttachShader(m_updateShader, vs);

	const char* varyings[] = { "position","velocity",
							  "lifetime", "lifespan" };
	glTransformFeedbackVaryings(m_updateShader, 4, varyings, GL_INTERLEAVED_ATTRIBS);
	glLinkProgram(m_updateShader);

	glDeleteShader(vs);

	glUseProgram(m_updateShader);

	int location = glGetUniformLocation(m_updateShader, "lifeMin");
	glUniform1f(location, m_lifespanMin);
	location = glGetUniformLocation(m_updateShader, "lifeMax");
	glUniform1f(location, m_lifespanMax);
}

void GPUParticleEmitter::CreateDrawShader()
{
	Shader drawShader("shaders/GPUparticleV.txt", "shaders/GPUparticleF.txt", "shaders/GPUparticleG.txt");
	//load shaders from files and turn into GLshader handles
	static const char* vertex_shader = drawShader.m_vertex.c_str();
	static const char* fragment_shader = drawShader.m_fragment.c_str();
	static const char* geometry_shader = drawShader.m_geometry.c_str();

	GLuint vs = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs, 1, &vertex_shader, NULL);
	glCompileShader(vs);

	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, &fragment_shader, NULL);
	glCompileShader(fs);

	GLuint gs = glCreateShader(GL_GEOMETRY_SHADER);
	glShaderSource(gs, 1, &geometry_shader, NULL);
	glCompileShader(gs);

	m_drawShader = glCreateProgram();
	glAttachShader(m_drawShader, vs);
	glAttachShader(m_drawShader, fs);
	glAttachShader(m_drawShader, gs);
	glLinkProgram(m_drawShader);

	glDeleteShader(vs);
	glDeleteShader(fs);
	glDeleteShader(gs);

	glUseProgram(m_drawShader);

	int location = glGetUniformLocation(m_drawShader, "sizeStart");
	glUniform1f(location, m_startSize);
	location = glGetUniformLocation(m_drawShader, "sizeEnd");
	glUniform1f(location, m_endSize);

	location = glGetUniformLocation(m_drawShader, "colorStart");
	glUniform4fv(location, 1, &m_startColor[0]);
	location = glGetUniformLocation(m_drawShader, "colorEnd");
	glUniform4fv(location, 1, &m_endColor[0]);
}

void ExplosionEmitter::emit()
{
	//only emit if there is a dead particle to use
	if (m_firstDead >= m_maxParticles)
		return;

	//resurrect the first dead particle
	Particle& particle = m_particles[m_firstDead++];

	//assign its starting position
	particle.m_position = m_position;

	//randomise its lifespan
	particle.m_lifetime = 0;
	particle.m_lifespan = (rand() / (float)RAND_MAX) * (m_lifespanMax - m_lifespanMin) + m_lifespanMin;

	//set starting size and color
	particle.m_color = m_startColor;
	particle.m_size = m_startSize;
	
	//randomise velocity direction and strength
	float velocity = (rand() / (float)RAND_MAX)*(m_velocityMax - m_velocityMin) + m_velocityMin;
	particle.m_velocity.x = (rand() / (float)RAND_MAX) * 2 - 1;
	particle.m_velocity.y = (rand() / (float)RAND_MAX) * 2 - 1;
	particle.m_velocity.z = (rand() / (float)RAND_MAX) * 2 - 1;
	particle.m_velocity = glm::normalize(particle.m_velocity) * velocity;
}

void ExplosionEmitter::update(float a_deltaTime, Camera * a_camera, vec3 a_emitterParticle)
{
	//spawn particles
	m_emitTimer += a_deltaTime;
	while (m_emitTimer > m_emitRate)
	{
		emit();
		m_emitTimer -= m_emitRate;
	}
	m_position = a_emitterParticle;
	unsigned int quad = 0;

	//update particles and turn live particles into billboard quads
	for (unsigned int i = 0; i < m_firstDead; i++)
	{
		Particle* particle = &m_particles[i];

		m_currentParticles.at(i) = particle; //assign Particle at index i the information of the particle variable

		m_direction.y -= 0.01;

		particle->m_lifetime += a_deltaTime;
		if (particle->m_lifetime >= particle->m_lifespan)
		{
			//swap last alive with this one
			*particle = m_particles[m_firstDead - 1];
			m_firstDead--;			
		}
		else
		{
			//move particle
			particle->m_velocity += m_direction;
			particle->m_position += particle->m_velocity * a_deltaTime;
			//size particle
			particle->m_size = glm::mix(m_startSize, m_endSize, particle->m_lifetime / particle->m_lifespan);

			//color particle
			particle->m_color = glm::mix(m_startColor, m_endColor, particle->m_lifetime / particle->m_lifespan);

			//make a quad the correct size and color
			float halfSize = particle->m_size * 0.5f;

			m_vertexData[quad * 4 + 0].position = vec4(halfSize, halfSize, 0, 1);
			m_vertexData[quad * 4 + 0].colour = particle->m_color;
			m_vertexData[quad * 4 + 1].position = vec4(-halfSize, halfSize, 0, 1);
			m_vertexData[quad * 4 + 1].colour = particle->m_color;
			m_vertexData[quad * 4 + 2].position = vec4(-halfSize, -halfSize, 0, 1);
			m_vertexData[quad * 4 + 2].colour = particle->m_color;
			m_vertexData[quad * 4 + 3].position = vec4(halfSize, -halfSize, 0, 1);
			m_vertexData[quad * 4 + 3].colour = particle->m_color;

			//create billboard transform
			vec3 zAxis = glm::normalize(a_camera->GetPosition() - particle->m_position);
			vec3 xAxis = glm::cross(vec3(a_camera->GetView()[1]), zAxis);
			vec3 yAxis = glm::cross(zAxis, xAxis);
			mat4 billboard(vec4(xAxis, 0),
				vec4(yAxis, 0),
				vec4(zAxis, 0),
				vec4(0, 0, 0, 1));

			m_vertexData[quad * 4 + 0].position = billboard * m_vertexData[quad * 4 + 0].position + vec4(particle->m_position, 0);
			m_vertexData[quad * 4 + 1].position = billboard * m_vertexData[quad * 4 + 1].position + vec4(particle->m_position, 0);
			m_vertexData[quad * 4 + 2].position = billboard * m_vertexData[quad * 4 + 2].position + vec4(particle->m_position, 0);
			m_vertexData[quad * 4 + 3].position = billboard * m_vertexData[quad * 4 + 3].position + vec4(particle->m_position, 0);

			++quad;
		}
	}
}
