#pragma once

#include "Application.h"
#include <glm\mat4x4.hpp>
#include <glm\vec2.hpp>
#include <glm\vec3.hpp>
#include <glm\mat3x3.hpp>
#include <Texture.h>
#include "../FBXModelLoading.h"
#include "../Particles.h"
#include <vector>
#include "../Bounding.h"
class Camera;
class Shader;
class PostProcessor;

class GLMesh
{
public:
	GLMesh();
	~GLMesh();


	void CreateRenderQuad(unsigned int a_width, unsigned int a_height);

	void Draw();

	unsigned int m_Vao;
	unsigned int m_Vbo;
	unsigned int m_Ibo;
	unsigned int m_indicesCount;
};

struct Vertex
{
	//Vertex();
	//~Vertex();

	glm::vec4 pos;
	glm::vec2 uv;
	glm::vec3 norm;

	static void SetupVertexAttribPointers();
};

struct Light
{
	glm::vec3 m_vLightPosition;
	glm::vec3 m_vLightColor;
	glm::vec3 m_vSpecularColor;
	float m_fLightAmbientStrength;
	float m_fLightStrength;
};

class LandscapeApp : public aie::Application {
public:


	LandscapeApp();
	virtual ~LandscapeApp();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

	void LoadTextures();

	void LoadShader(const char* a_vs, const char* a_fs);
	void UnloadShader();

	void CreateLandscape();
	void DestroyLandscape();

	void ShaderErrorCheck(unsigned int a_shader);

	void CreateGUI();

	glm::vec3 MakeNormalVector(glm::vec4 finish, glm::vec4 start);

protected:

	

	Camera *m_cCamera;
	aie::Texture *m_tTexture;
	aie::Texture *m_tHeightMap;

	/*Challenge Stuff*/
	aie::Texture *m_splatMap;
	aie::Texture *m_grass;
	aie::Texture *m_rock;
	aie::Texture *m_sand;
	aie::Texture *m_snow;
	aie::Texture *m_smoke;

	glm::mat4	m_viewMatrix;
	glm::mat4	m_projectionMatrix;

	Light m_lLight;
	FBXLoader m_Mesh;

	GLMesh postQuad;

	ParticleEmitter *m_emitter;
	std::vector<ParticleEmitter*> m_trails;
	std::vector<ExplosionEmitter*> m_explosionData;
	ExplosionEmitter* m_explosion;
	PostProcessor *m_Processor;

	BoundingSphere sphere;

	std::string frustum;

	unsigned int m_shader;

	unsigned int m_IndicesCount;
	unsigned int m_iVertCount;

	unsigned int m_Vao;
	unsigned int m_Vbo;
	unsigned int m_Ibo;

	const unsigned int m_iLandWidth = 512;
	const unsigned int m_iLandLength = 512;
	const float m_fLandHeight = 1.0f;
	float m_tempHeight = 1.0f;
	const float m_fVertDistance = 0.1f;

	bool m_bLightRotation = false;
	bool m_bAmbientLighting = true;
	bool m_bDiffuseLighting = true;
	bool m_bSpecularLighting = false;
	bool m_bLoadTexture = true;

};