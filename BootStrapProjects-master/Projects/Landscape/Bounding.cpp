#include "Bounding.h"

AABB::AABB()
{
	reset();
}

AABB::~AABB()
{
}

void AABB::reset()
{
	m_min.x = m_min.y = m_min.z = 1e37f;
	m_max.x = m_max.y = m_max.z = -1e37f;
}

void AABB::fit(const std::vector<glm::vec3>& points)
{
	for (auto& p : points) 
	{ 
		if (p.x < m_min.x) m_min.x = p.x; 
		if (p.y < m_min.y) m_min.y = p.y; 
		if (p.z < m_min.z) m_min.z = p.z; 
		if (p.x > m_max.x) m_max.x = p.x; 
		if (p.y > m_max.y) m_max.y = p.y; 
		if (p.z > m_max.z) m_max.z = p.z; 
	}
}


BoundingSphere::BoundingSphere()
	: m_centre(0),
	m_radius(0)
{
}
BoundingSphere::~BoundingSphere()
{
}

void BoundingSphere::fit(const std::vector<glm::vec3>& points)
{
	glm::vec3 min(1e37f), max(-1e37f);

	for (auto& p : points) 
	{ 
		if (p.x < min.x) min.x = p.x; 
		if (p.y < min.y) min.y = p.y; 
		if (p.z < min.z) min.z = p.z; 
		if (p.x > max.x) max.x = p.x; 
		if (p.y > max.y) max.y = p.y; 
		if (p.z > max.z) max.z = p.z; 
	}
	m_centre = (min + max) * 0.5f;
	m_radius = glm::distance(min, m_centre);
}
