#ifndef _BOUNDING_H_
#define _BOUNDING_H_

#include <glm\mat4x4.hpp>
#include <glm\vec2.hpp>
#include <glm\vec3.hpp>
#include <glm\glm.hpp>
#include <glm\ext.hpp>
#include <Texture.h>
#include <vector>

class AABB
{
public:
	AABB();
	~AABB();

	void reset();

	void fit(const std::vector<glm::vec3>& points);
	
	glm::vec3 m_min, m_max;
private:
	
};

class BoundingSphere
{
public:
	BoundingSphere();
	~BoundingSphere();

	void fit(const std::vector<glm::vec3>& points);
	glm::vec3	m_centre;
	float		m_radius;
private:
	
};
#endif