#ifndef _POSTPROCESSING_H_
#define	_POSTPROCESSING_H_
#include <vector>
#include <glm\vec2.hpp>
#include <../Camera.h>

class PostProcessor
{
public:
	PostProcessor();
	PostProcessor(unsigned int a_width, unsigned int a_height);
	~PostProcessor();

	void SetupFrameBuffer();
	void DrawFrameBuffer();
	void LoadPostShader(const char* a_vs, const char* a_fs);
	void UpdateScreenSize(unsigned int a_width, unsigned int a_height);

	void StartProcessing();
	void EndProcessing();

	unsigned int m_PostVao;
	unsigned int m_PostVbo;
	unsigned int m_fbo;
	unsigned int m_fboTexture;
	unsigned int m_fboDepth;
	unsigned int m_postShader;

	unsigned int m_windowWidth;
	unsigned int m_windowHeight;

	glm::vec2 halfTexel;
};


#endif // !_POSTPROCESSING_H_
