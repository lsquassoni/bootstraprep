#include "FBXModelLoading.h"
#include "gl_core_4_4.h"
FBXLoader::FBXLoader()
{
}

FBXLoader::~FBXLoader()
{
}

void FBXLoader::CreateFBXOpenGLBuffers(FBXFile * a_file)
{
	for (unsigned int i = 0; i < a_file->getMeshCount(); i++)
	{
		// get the current mesh from file
		FBXMeshNode *fbxMesh = a_file->getMeshByIndex(i);
		FBXLoader *glData = new FBXLoader();
		glGenVertexArrays(1, &glData->vao);
		glBindVertexArray(glData->vao);
		glGenBuffers(1, &glData->vbo);
		glGenBuffers(1, &glData->ibo);
		glBindBuffer(GL_ARRAY_BUFFER, glData->vbo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glData->ibo);
		// fill the vbo with our vertices.
		// the FBXLoader has convinently already defined a Vertex Structure for us.
		glBufferData(GL_ARRAY_BUFFER,
			fbxMesh->m_vertices.size() * sizeof(FBXVertex),
			fbxMesh->m_vertices.data(), GL_STATIC_DRAW);
		// fill the ibo with the indices.
		// fbx meshes can be large, so indices are stored as an unsigned int.
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,
			fbxMesh->m_indices.size() * sizeof(unsigned int),
			fbxMesh->m_indices.data(), GL_STATIC_DRAW);
		// Setup Vertex Attrib pointers
		// remember, we only need to setup the approprate attributes for the shaders that will be rendering
		// this fbx object.
		//glEnableVertexAttribArray(0); // position
		//glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), 0);
		//glEnableVertexAttribArray(1); // normal
		//glVertexAttribPointer(1, 4, GL_FLOAT, GL_TRUE, sizeof(FBXVertex), ((char*)0) + FBXVertex::NormalOffset);
		//glEnableVertexAttribArray(2); // uv
		//glVertexAttribPointer(2, 2, GL_FLOAT, GL_TRUE, sizeof(FBXVertex), ((char*)0) + FBXVertex::TexCoord1Offset);
		glEnableVertexAttribArray(0); //position
		glEnableVertexAttribArray(1); //normals
		glEnableVertexAttribArray(2); //tangents
		glEnableVertexAttribArray(3); //texcoords
		glEnableVertexAttribArray(4); //weights
		glEnableVertexAttribArray(5); //indices

		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)FBXVertex::PositionOffset);
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)FBXVertex::NormalOffset);
		glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)FBXVertex::TangentOffset);
		glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)FBXVertex::TexCoord1Offset);
		glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)FBXVertex::WeightsOffset);
		glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), (void*)FBXVertex::IndicesOffset);

		// TODO: add any additional attribute pointers required for shader use.
		// unbind
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		// attach our GLMesh object to the m_userData pointer.
		fbxMesh->m_userData = glData;
	}
}

void FBXLoader::CleanupFBXOpenGLBuffers(FBXFile * a_file)
{
	for (unsigned int i = 0; i < a_file->getMeshCount(); i++)
	{
		FBXMeshNode *fbxMesh = a_file->getMeshByIndex(i);
		FBXLoader *glData = (FBXLoader *)fbxMesh->m_userData;
		glDeleteVertexArrays(1, &glData->vao);
		glDeleteBuffers(1, &glData->vbo);
		glDeleteBuffers(1, &glData->ibo);
		delete glData;
	}
}

void FBXLoader::LoadShaders(const char* a_vS, const char* a_fS)
{
	const char* vsSource = a_vS;
	const char* fsSource = a_fS;

	unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, (const char**)&vsSource, 0);
	glCompileShader(vertexShader);
	unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, (const char**)&fsSource, 0);
	glCompileShader(fragmentShader);
	m_shader = glCreateProgram();
	glAttachShader(m_shader, vertexShader);
	glAttachShader(m_shader, fragmentShader);
	glLinkProgram(m_shader);
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
}

void FBXLoader::UnloadShaders()
{
	glDeleteProgram(m_shader);
}

void FBXLoader::Update(float deltaTime)
{
	FBXSkeleton* skeleton = m_myFbxModel->getSkeletonByIndex(0);
	FBXAnimation* animation = m_myFbxModel->getAnimationByIndex(0);

	skeleton->evaluate(animation, m_fbxAnimationTime);

	m_fbxAnimationTime += deltaTime;
	for (unsigned int i = 0; i < skeleton->m_boneCount; ++i)
	{
		skeleton->m_nodes[i]->updateGlobalTransform();
	}	
}

void FBXLoader::draw(glm::mat4 a_ProjectView, glm::vec3 a_lightPos, glm::vec3 a_lightColor, glm::vec3 a_specColor, glm::vec3 a_cameraPos, float a_lightAmb, float a_lightStrength)
{
	// scale matrix to reduce the models size
	// some models are very large, some are smaller, so this can be helpfull.
	float s = 1.0f / 150.0f;
	glm::mat4 model = glm::mat4(
		s, 0, 0, 0,
		0, s, 0, 0,
		0, 0, s, 0,
		0, 3, -3, 1
	);
	glm::mat4 modelViewProjection = a_ProjectView * model;
	glUseProgram(m_shader);

	FBXSkeleton* skeleton = m_myFbxModel->getSkeletonByIndex(0);
	skeleton->updateBones();

	int bones_location = glGetUniformLocation(m_shader, "bones");
	glUniformMatrix4fv(bones_location, skeleton->m_boneCount, GL_FALSE, (float*)skeleton->m_bones);
	// send uniform variables, in this case the "projectionViewWorldMatrix"
	unsigned int mvpLoc = glGetUniformLocation(m_shader, "projectionView");
	glUniformMatrix4fv(mvpLoc, 1, GL_FALSE, &modelViewProjection[0][0]);
	// loop through each mesh within the fbx file
	for (unsigned int i = 0; i < m_myFbxModel->getMeshCount(); ++i)
	{
		FBXMeshNode* mesh = m_myFbxModel->getMeshByIndex(i);
		FBXLoader* glData = (FBXLoader*)mesh->m_userData;
		// get the texture from the model
		unsigned int diffuseTexture = m_myFbxModel->getTextureByIndex(mesh->m_material->DiffuseTexture);
		// bid the texture and send it to our shader
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, diffuseTexture);
		glUniform1i(glGetUniformLocation(m_shader, "diffuseTexture"), 0);

		glUniform3fv(glGetUniformLocation(m_shader, "lightPosition"), 1, &a_lightPos[0]);
		glUniform3fv(glGetUniformLocation(m_shader, "lightColor"), 1, &a_lightColor[0]);
		glUniform1fv(glGetUniformLocation(m_shader, "lightAmbientStrength"), 1, &a_lightAmb);
		glUniform1fv(glGetUniformLocation(m_shader, "lightStrength"), 1, &a_lightStrength);
		glUniform3fv(glGetUniformLocation(m_shader, "specularColor"), 1, &a_specColor[0]);
		glUniform3fv(glGetUniformLocation(m_shader, "cameraPosition"), 1, &a_cameraPos[0]);
		// draw the mesh
		glBindVertexArray(glData->vao);
		glDrawElements(GL_TRIANGLES, mesh->m_indices.size(), GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
	}
	glUseProgram(0);
}

void FBXLoader::SetShader(unsigned int *a_Shader)
{
	m_shader = *a_Shader;
}

void FBXLoader::SetVAO(unsigned int a_VAO)
{
	vao = a_VAO;
}

void FBXLoader::SetVBO(unsigned int a_VBO)
{
	vbo = a_VBO;
}

void FBXLoader::SetIBO(unsigned int a_IBO)
{
	ibo = a_IBO;
}

void FBXLoader::RotateModel(glm::mat4* a_modelMat, float a_angle)
{
	//float* mat = glm::value_ptr(a_modelMat);
}
