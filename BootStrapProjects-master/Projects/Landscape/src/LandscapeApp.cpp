/*
-------------------------------------------------------------------------------
INSTRUCTIONS:
-------------------------------------------------------------------------------
STEP 1: Load a shader program
See the LoadShader method

STEP 2: Generate Geometry
See the CreateGeometry method

STEP 3: Each Frame - Render Geometry (using the shader program)
See the DrawGeometry method

STEP 4: Unload Shader and Geometry
-------------------------------------------------------------------------------
*/

#include "LandscapeApp.h"
#include "Gizmos.h"
#include "Input.h"
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <imgui.h>

#include "gl_core_4_4.h"
#include "../Camera.h"
#include "../Shader.h"
#include "../PostProcessing.h"


using glm::vec3;
using glm::vec4;
using glm::mat4;
using aie::Gizmos;
using aie::Texture;

void LandscapeApp::LoadTextures()
{
	m_tHeightMap->load("textures/Forodwaith512.bmp");
	m_grass->load("textures/challenge/grass.png");
	m_rock->load("textures/challenge/rock.png");
	m_sand->load("textures/challenge/sand.png");
	m_snow->load("textures/challenge/snow.png");
	m_splatMap->load("textures/ForodSplat.bmp");
	m_smoke->load("textures/rock_large.png");
}

LandscapeApp::LandscapeApp() {

}

LandscapeApp::~LandscapeApp() {

}

bool LandscapeApp::startup() {

	setBackgroundColour(0.25f, 0.25f, 0.25f);

	// initialise gizmo primitive counts
	Gizmos::create(10000, 10000, 10000, 10000);

	/*Set up camera starting position and where it's looking*/
	m_tTexture = new aie::Texture();
	m_tHeightMap = new aie::Texture();
	m_grass = new aie::Texture();
	m_rock = new aie::Texture();
	m_sand = new aie::Texture();
	m_snow = new aie::Texture();
	m_splatMap = new aie::Texture();
	m_smoke = new aie::Texture();

	m_cCamera = new Camera();
	m_cCamera->SetPosition(glm::vec3(5.0f, 5.0f, 5.0f));
	m_cCamera->LookAt(glm::vec3(0.0f, 0.0f, 0.0f));

	m_lLight.m_vLightPosition = glm::vec3(0.0f, 20.0f, 0.0f);
	m_lLight.m_vLightColor = glm::vec3(1.0f, 1.0f, 0.8f);
	m_lLight.m_vSpecularColor = m_lLight.m_vLightColor;//glm::vec3(1.0f, 1.0f, 1.0f);
	m_lLight.m_fLightAmbientStrength = 1.0f;
	m_lLight.m_fLightStrength = 0.1f;

	m_projectionMatrix = glm::perspective(glm::pi<float>() * 0.25f,
										  getWindowWidth() / (float)getWindowHeight(),
										  0.1f, 1000.f);

	LoadTextures();

	Shader landscapeShader("shaders/landscapeV.vert", "shaders/landscapeF.frag");
	LoadShader(landscapeShader.m_vertex.c_str(),landscapeShader.m_fragment.c_str());
	CreateLandscape();

	m_Mesh.m_myFbxModel = new FBXFile();
	m_Mesh.m_myFbxModel->load("models/pyro/pyro.fbx", FBXFile::UNITS_CENTIMETER);
	m_Mesh.CreateFBXOpenGLBuffers(m_Mesh.m_myFbxModel);

	Shader soulspearShader("models/modelV.vert", "models/modelF.frag");
	m_Mesh.LoadShaders(soulspearShader.m_vertex.c_str(),soulspearShader.m_fragment.c_str());
	ShaderErrorCheck(m_Mesh.GetShader());

	Shader sParticle("shaders/particleV.vert", "shaders/particleF.frag");
	m_emitter = new ParticleEmitter();
	m_emitter->initialize(2, 2, 1.0f, 4.0f, 1.0f, 4.0f, 0.5f, 0.5f, glm::vec4(1, 0, 1, 1), glm::vec4(1, 0, 1, 1),glm::vec3(0,0.2,0));

	Shader smokeParticle("shaders/smokeParticleV.vert", "shaders/smokeParticleF.frag");

	for (unsigned int i = 0; i < m_emitter->GetMaxParticles(); i++)
	{
		ParticleEmitter* pE = new ParticleEmitter();
		m_trails.push_back(pE);
		m_trails.at(i)->initialize(100, 50, 0.1f, 1.0f, 1.0f, 4.0f, 0.5f, 1.0f, glm::vec4(1, 0, 0, 1), glm::vec4(1, 1, 0, 1), glm::vec3(0, 0, 0));
		m_trails.at(i)->SetTexture(m_smoke);
		m_trails.at(i)->loadShader(smokeParticle.m_vertex.c_str(), smokeParticle.m_fragment.c_str());
	}

	m_emitter->loadShader(sParticle.m_vertex.c_str(), sParticle.m_fragment.c_str());
	ShaderErrorCheck(m_emitter->m_particleShader);

	m_Processor = new PostProcessor(getWindowWidth(),getWindowHeight());
	Shader PostShader("shaders/frameBufferV.vert", "shaders/frameBufferF.frag");
	m_Processor->LoadPostShader(PostShader.m_vertex.c_str(), PostShader.m_fragment.c_str());	
	
	sphere.m_centre = vec3(0, 10, 0);
	sphere.m_radius = 0.5f;

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	return true;
}

void LandscapeApp::shutdown() {
	DestroyLandscape();
	UnloadShader();

	ImGui::Shutdown();

	m_Mesh.UnloadShaders();
	m_Mesh.CleanupFBXOpenGLBuffers(m_Mesh.m_myFbxModel);
	m_Mesh.m_myFbxModel->unload();
	delete m_Mesh.m_myFbxModel;

	Gizmos::destroy();
	delete m_cCamera;
}

void LandscapeApp::update(float deltaTime) {

	// query time since application started
	float time = getTime();
		
	// update perspective in case window resized
	m_projectionMatrix = glm::perspective(glm::pi<float>() * 0.25f,
		getWindowWidth() / (float)getWindowHeight(),
		0.1f, 1000.f);
	
	vec4 planes[6];
	m_cCamera->GetFrustumPlanes(m_projectionMatrix * (m_cCamera->GetView()), planes);

	for (int i = 0; i < 6; i++)
	{
		float d = glm::dot(vec3(planes[i]), sphere.m_centre) + planes[i].w;

		if (d < -sphere.m_radius)
		{
			frustum = "Behind, don't render it.";
		}
		else
		{
			frustum = "Front, fully visible so render it.";
		}
	}

	CreateGUI();

	m_Mesh.Update(deltaTime);

	//Control camera
	m_cCamera->Update(deltaTime);

	m_emitter->update(deltaTime, m_cCamera, vec3(0,m_tempHeight,0));

	for (unsigned int i = 0; i < m_trails.size(); i++)
	{
		m_trails.at(i)->update(deltaTime, m_cCamera, m_emitter->m_currentParticles.at(i)->m_position);
	}	

	m_Processor->UpdateScreenSize(getWindowWidth(), getWindowHeight());

	//Animate light
	if (m_bLightRotation == true)
		m_lLight.m_vLightPosition = vec3(glm::sin(time / 10) * 100, glm::cos(time / 10) * 50, 0);
	else
		m_lLight.m_vLightPosition = vec3(0, m_lLight.m_vLightPosition.y, 0);

	// wipe the gizmos clean for this frame
	Gizmos::clear();

	// quit if we press escape
	aie::Input* input = aie::Input::getInstance();
	//ImGui::Render();

	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();
}

void LandscapeApp::draw() {
	
	// wipe the screen to the background colour
	clearScreen();

	//glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
	
	m_Processor->StartProcessing();

	//DrawGrid();

	vec4 sphereColor = vec4(m_lLight.m_vLightColor.x, m_lLight.m_vLightColor.y, m_lLight.m_vLightColor.z, 1.0f);
	Gizmos::addSphere(m_lLight.m_vLightPosition, 0.05f, 10, 10, sphereColor);

	// STEP 1: enable the shader program for rendering
	glUseProgram(m_shader);

	// Step 2: send uniform variables to the shader
	glm::mat4 projectionView = m_projectionMatrix * m_cCamera->GetView();
	glUniformMatrix4fv(
		glGetUniformLocation(m_shader, "projectionView"), 
		1, 
		false, 
		glm::value_ptr(projectionView));

	glActiveTexture(GL_TEXTURE0);
	/*Bind texture*/
	glBindTexture(GL_TEXTURE_2D, m_tTexture->getHandle());
	glUniform1i(glGetUniformLocation(m_shader, "tileTexture"), 0);

	/*Set up my grass texture*/
	glActiveTexture(GL_TEXTURE1); /*Grass*/
	glBindTexture(GL_TEXTURE_2D, m_grass->getHandle());
	glUniform1i(glGetUniformLocation(m_shader, "grass"), 1);

	glActiveTexture(GL_TEXTURE4); /*Snow*/
	glBindTexture(GL_TEXTURE_2D, m_snow->getHandle());
	glUniform1i(glGetUniformLocation(m_shader, "snow"), 4);

	glActiveTexture(GL_TEXTURE5); /*Snow*/
	glBindTexture(GL_TEXTURE_2D, m_splatMap->getHandle());
	glUniform1i(glGetUniformLocation(m_shader, "splatMap"), 5);

	/*Set up Light/Color Information*/
	glUniform3fv(glGetUniformLocation(m_shader, "lightPosition"), 1, &m_lLight.m_vLightPosition[0]);
	glUniform3fv(glGetUniformLocation(m_shader, "lightColor"), 1, &m_lLight.m_vLightColor[0]);
	glUniform1fv(glGetUniformLocation(m_shader, "lightAmbientStrength"), 1, &m_lLight.m_fLightAmbientStrength);
	glUniform1fv(glGetUniformLocation(m_shader, "lightStrength"), 1, &m_lLight.m_fLightStrength);
	glUniform3fv(glGetUniformLocation(m_shader, "specularColor"), 1, &m_lLight.m_vSpecularColor[0]);
	glUniform3fv(glGetUniformLocation(m_shader, "cameraPosition"), 1, &m_cCamera->GetPosition()[0]);
	

	// Step 3: Bind the VAO
	// When we setup the geometry, we did a bunch of glEnableVertexAttribArray and glVertexAttribPointer method calls
	// we also Bound the vertex array and index array via the glBindBuffer call.
	// if we where not using VAO's we would have to do thoes method calls each frame here.
	glBindVertexArray(m_Vao);

	// Step 4: Draw Elements. We are using GL_TRIANGLES.
	// we need to tell openGL how many indices there are, and the size of our indices
	// when we setup the geometry, our indices where an unsigned char (1 byte for each indicy)
	glDrawElements(GL_TRIANGLES, m_IndicesCount, GL_UNSIGNED_INT, 0);
	//DrawLandscape();

	// Step 5: Now that we are done drawing the geometry
	// unbind the vao, we are basicly cleaning the opengl state
	glBindVertexArray(0);

	// Step 6: de-activate the shader program, dont do future rendering with it any more.
	glUseProgram(0);	

	m_Mesh.draw(projectionView, m_lLight.m_vLightPosition, m_lLight.m_vLightColor, 
		m_lLight.m_vSpecularColor, m_cCamera->GetPosition(), m_lLight.m_fLightAmbientStrength, m_lLight.m_fLightStrength);

	m_emitter->draw(projectionView);

	for (unsigned int i = 0; i < m_trails.size(); i++)
	{
		m_trails.at(i)->draw(projectionView);
	}

	Gizmos::addSphere(sphere.m_centre, sphere.m_radius, 8, 8, vec4(1, 0, 0, 1));

	m_Processor->EndProcessing();
	m_Processor->DrawFrameBuffer();
	

}

void LandscapeApp::LoadShader(const char* a_vs, const char* a_fs)
{
	static const char* vertex_shader = a_vs;
	static const char* fragment_shader = a_fs;

	// Step 1:
	// Load the vertex shader, provide it with the source code and compile it.
	GLuint vs = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs, 1, &vertex_shader, NULL);
	glCompileShader(vs);

	// Step 2:
	// Load the fragment shader, provide it with the source code and compile it.
	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, &fragment_shader, NULL);
	glCompileShader(fs);

	// step 3:
	// Create the shader program
	m_shader = glCreateProgram();

	// Step 4:
	// attach the vertex and fragment shaders to the m_shader program
	glAttachShader(m_shader, vs);
	glAttachShader(m_shader, fs);

	// Step 5:
	// describe the location of the shader inputs the link the program
	glLinkProgram(m_shader);

	ShaderErrorCheck(m_shader);

	// step 6:
	// delete the vs and fs shaders
	glDeleteShader(vs);
	glDeleteShader(fs);


}

void LandscapeApp::UnloadShader()
{
	glDeleteProgram(m_shader);
}

void Vertex::SetupVertexAttribPointers()
{
	// enable vertex position element
	// notice when we loaded the shader, we described the "position" element to be location 0.
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(
		0,                  // attribute 0 (position)
		4,                  // size - how many floats make up the position (x, y, z, w)
		GL_FLOAT,           // type - our x,y,z, w are float values
		GL_FALSE,           // normalized? - not used
		sizeof(Vertex),     // stride - size of an entire vertex
		(void*)0            // offset - bytes from the beginning of the vertex
	);

	// enable vertex UV element
	// notice when we loaded the shader, we described the "color" element to be location 1.
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(
		1,                  // attribute 1 (color)
		2,                  // size - how many floats make up the UV (U, V)
		GL_FLOAT,           // type - our x,y,z are float values
		GL_FALSE,           // normalized? - not used
		sizeof(Vertex),     // stride - size of an entire vertex
		(void*)(sizeof(float) * 4)            // offset - bytes from the beginning of the vertex
	);
	// enable vertex UV element
	// notice when we loaded the shader, we described the "color" element to be location 1.
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(
		2,                  // attribute 1 (color)
		4,                  // size - how many floats make up the Normal (x, y, z, w)
		GL_FLOAT,           // type - our x,y,z are float values
		GL_FALSE,           // normalized? - not used
		sizeof(Vertex),     // stride - size of an entire vertex
		(void*)(sizeof(float) * 6)            // offset - bytes from the beginning of the vertex
	);
}

void LandscapeApp::CreateLandscape()
{
	std::vector<Vertex> vVerts;
	std::vector<unsigned int> vIndices;

	const unsigned char *pixels = m_tHeightMap->getPixels();

	/*Create grid of verts*/
	for (int z = 0; z < m_iLandLength; z++)
	{
		for (int x = 0; x < m_iLandWidth; x++)
		{
			int sampleX = (int)((float)x / m_iLandWidth * m_tHeightMap->getWidth());
			int sampleZ = (int)((float)z / m_iLandLength * m_tHeightMap->getHeight());

			int i = (sampleZ * m_tHeightMap->getWidth() + sampleX);

			/*Position of vertex*/
			float fXPos = (x * m_fVertDistance) - (m_iLandWidth * m_fVertDistance * 0.5f);
			float fYPos = (pixels[i * 3] / 255.0f) * m_tempHeight;
			float fZPos = (z * m_fVertDistance) - (m_iLandLength * m_fVertDistance * 0.5f);

			float u = (float)x / (m_iLandWidth - 1);
			float v = (float)z / (m_iLandLength - 1);

			Vertex vert{
				{fXPos, fYPos, fZPos, 1.0f}, /*Position*/
				{u, v},						 /*TexCoords*/
				{0.0f, 0.0f, 0.0f}	 /*Normals*/
			};
			vVerts.push_back(vert);
		}

	}
	/*Calculate indices for triangles*/
	for (int z = 0; z < m_iLandLength - 1; z++)
	{
		for (int x = 0; x < m_iLandWidth - 1; x++)
		{
			int i = z * m_iLandWidth + x; /*The address of the vertices in the single dimension vector*/

			/*Triangulate*/
			vIndices.push_back(i + 1);
			vIndices.push_back(i);
			vIndices.push_back(i + m_iLandWidth);
			
			/*Vector of i*/
			glm::vec3 firstVec = MakeNormalVector(vVerts[i + m_iLandWidth].pos, vVerts[i].pos);
			glm::vec3 SecondVec = MakeNormalVector(vVerts[i + 1].pos, vVerts[i].pos);

			vVerts[i].norm += glm::cross(firstVec, SecondVec);

			vVerts[i + m_iLandWidth].norm += glm::cross(firstVec, SecondVec);

			vVerts[i + 1].norm += glm::cross(firstVec, SecondVec);

	//--------------------------------------------------------------------------
			vIndices.push_back(i + 1);
			vIndices.push_back(i + m_iLandWidth);
			vIndices.push_back(i + m_iLandWidth + 1);
			
			/*Vector of i + m_iLandWidth*/
			firstVec = MakeNormalVector(vVerts[i + m_iLandWidth + 1].pos, vVerts[i + m_iLandWidth].pos);
			SecondVec = MakeNormalVector(vVerts[i + 1].pos, vVerts[i + m_iLandWidth].pos);

			vVerts[i].norm += glm::cross(firstVec, SecondVec);
			
			vVerts[i + m_iLandWidth].norm += glm::cross(firstVec, SecondVec);
			
			vVerts[i + 1].norm += glm::cross(firstVec, SecondVec);

			glm::normalize(vVerts[i].norm);
		}
	}

	m_iVertCount = vVerts.size();
	m_IndicesCount = vIndices.size();

	glGenVertexArrays(1, &m_Vao);
	glBindVertexArray(m_Vao);

	// Create our VBO and IBO.
	// Then tell Opengl what type of buffer they are used for
	// VBO a buffer in graphics memory to contains our vertices
	// IBO a buffer in graphics memory to contain our indices.
	// Then Fill the buffers with our generated data.
	// This is taking our verts and indices from ram, and sending them to the graphics card
	glGenBuffers(1, &m_Vbo);
	glGenBuffers(1, &m_Ibo);

	glBindBuffer(GL_ARRAY_BUFFER, m_Vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_Ibo);

	glBufferData(GL_ARRAY_BUFFER, m_iVertCount * sizeof(Vertex), &vVerts[0], GL_STATIC_DRAW);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_IndicesCount * sizeof(unsigned int), &vIndices[0], GL_STATIC_DRAW);

	Vertex::SetupVertexAttribPointers();

	/*Unbind things after we've finished*/
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void LandscapeApp::DestroyLandscape()
{
	// When We're Done, destroy the geometry
	glDeleteBuffers(1, &m_Ibo);
	glDeleteBuffers(1, &m_Vbo);
	glDeleteVertexArrays(1, &m_Vao);
}

void LandscapeApp::ShaderErrorCheck(unsigned int a_shader)
{
	int success = GL_TRUE;
	glGetProgramiv(a_shader, GL_LINK_STATUS, &success);
	if (success == GL_FALSE)
	{
		int infoLogLength = 0;
		glGetProgramiv(a_shader, GL_INFO_LOG_LENGTH, &infoLogLength);

		char* error = new char[infoLogLength + 1];
		glGetProgramInfoLog(a_shader, infoLogLength, 0, error);
		printf("Shader Error:\n%s\n", error);
		delete[] error;
	}
}

void LandscapeApp::CreateGUI()
{
/*--------------------------------------------------------------------*/
	/*Master Window*/

	ImGui::Begin("Master Menu");
	ImGui::BeginChild("Terrain Options", ImVec2(0, 100), true);
	ImGui::Text("Mountain Height Settings");
	ImGui::Spacing();
	if (ImGui::Button("Increase Height")) { m_tempHeight++; CreateLandscape(); }
	ImGui::SameLine();
	if (ImGui::Button("Decrease Height")) { m_tempHeight--; CreateLandscape(); }
	if (ImGui::Button("Reset World Height")){ m_tempHeight = m_fLandHeight; CreateLandscape(); }
	ImGui::EndChild();
/*--------------------------------------------------------------------*/
	ImGui::BeginChild("Light Options", ImVec2(0, 180), true);
	ImGui::Text("Light Settings");
	ImGui::Spacing();
	ImGui::Text("Change Color");
	ImGui::ColorEdit3("", glm::value_ptr(m_lLight.m_vLightColor));
	ImGui::SliderFloat("Light Y", &m_lLight.m_vLightPosition.y, 1.0f, 20.0f, "%.1f");
	ImGui::Spacing();
	ImGui::InputFloat("Light Intensity", &m_lLight.m_fLightStrength);
	ImGui::Checkbox("Toggle Light Animation", &m_bLightRotation);
	ImGui::EndChild();
/*--------------------------------------------------------------------*/
	ImGui::Text(frustum.c_str());
	ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
	ImGui::End();

}

glm::vec3 LandscapeApp::MakeNormalVector(glm::vec4 finish, glm::vec4 start)
{
	return (glm::vec3)(finish - start);
}

GLMesh::GLMesh()
{
}

GLMesh::~GLMesh()
{
}

void GLMesh::CreateRenderQuad(unsigned int a_width, unsigned int a_height)
{
	glm::vec2 halfTexel = 1.0f / glm::vec2(a_width, a_height) * 0.5f;

	float verts[] = {
		-1, -1, 0, 1 , 	  halfTexel.x,     halfTexel.y ,
		 1,  1, 0, 1 ,  1 - halfTexel.x, 1 - halfTexel.y ,
		-1,  1, 0, 1 , 	  halfTexel.x, 1 - halfTexel.y ,
		  
		-1, -1, 0, 1 , 	  halfTexel.x,     halfTexel.y ,
		 1, -1, 0, 1 ,  1 - halfTexel.x,	   halfTexel.y,
		 1,  1, 0, 1 ,  1 - halfTexel.x, 1 - halfTexel.y,
	};

	glGenVertexArrays(1, &m_Vao);
	glBindVertexArray(m_Vao);

	glGenBuffers(1, &m_Vbo);
	glBindBuffer(GL_ARRAY_BUFFER, m_Vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*6*6, &verts, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(
		0,
		4,
		GL_FLOAT,
		GL_FALSE,
		sizeof(float) * 6,
		0
	);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(
		1,
		2,
		GL_FLOAT,
		GL_FALSE,
		sizeof(float) * 6,
		((char*)0) + 16		
	);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void GLMesh::Draw()
{
	glBindVertexArray(m_Vao);

	// Step 4: Draw Elements. We are using GL_TRIANGLES.
	// we need to tell openGL how many indices there are, and the size of our indices
	// when we setup the geometry, our indices where an unsigned char (1 byte for each indicy)
	glDrawElements(GL_TRIANGLES, m_indicesCount, GL_UNSIGNED_INT, 0);
	//DrawLandscape();

	// Step 5: Now that we are done drawing the geometry
	// unbind the vao, we are basicly cleaning the opengl state
	glBindVertexArray(0);
}
