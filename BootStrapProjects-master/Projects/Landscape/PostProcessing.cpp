#include "PostProcessing.h"
#include <gl_core_4_4.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>

PostProcessor::PostProcessor()
{
	m_windowWidth = 0;
	m_windowHeight = 0;
	SetupFrameBuffer();
}

PostProcessor::PostProcessor(unsigned int a_width, unsigned int a_height)
{
	m_windowWidth = a_width;
	m_windowHeight = a_height;
	SetupFrameBuffer();
}

PostProcessor::~PostProcessor()
{
}

void PostProcessor::SetupFrameBuffer()
{
	// setup framebuffer 
	glGenFramebuffers(1, &m_fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
	glGenTextures(1, &m_fboTexture);
	glBindTexture(GL_TEXTURE_2D, m_fboTexture);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, m_windowWidth, m_windowHeight);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_fboTexture, 0);
	glGenRenderbuffers(1, &m_fboDepth);
	glBindRenderbuffer(GL_RENDERBUFFER, m_fboDepth);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, m_windowWidth, m_windowHeight);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_fboDepth);
	GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, drawBuffers);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// fullscreen quad 
	halfTexel = 1.0f / glm::vec2(m_windowWidth, m_windowHeight) * 0.5f;
	float vertexData[] = {
		-1, -1, 0, 1, halfTexel.x, halfTexel.y,
		1, 1, 0, 1, 1 - halfTexel.x, 1 - halfTexel.y,
		-1, 1, 0, 1, halfTexel.x, 1 - halfTexel.y,
		-1, -1, 0, 1, halfTexel.x, halfTexel.y,
		1, -1, 0, 1, 1 - halfTexel.x, halfTexel.y,
		1, 1, 0, 1, 1 - halfTexel.x, 1 - halfTexel.y,
	};
	glGenVertexArrays(1, &m_PostVao);
	glBindVertexArray(m_PostVao);
	glGenBuffers(1, &m_PostVbo);
	glBindBuffer(GL_ARRAY_BUFFER, m_PostVbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 6 * 6, vertexData, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 6, 0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 6, ((char*)0) + 16);
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void PostProcessor::DrawFrameBuffer()
{
	// draw our full-screen quad 
	glUseProgram(m_postShader);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_fboTexture);
	int loc = glGetUniformLocation(m_postShader, "target");
	glUniform1i(loc, 0); 
	glBindVertexArray(m_PostVao);
	glDrawArrays(GL_TRIANGLES, 0, 6);
}

void PostProcessor::LoadPostShader(const char * a_vs, const char * a_fs)
{
	static const char* vertex_shader = a_vs;
	static const char* fragment_shader = a_fs;

	// Step 1:
	// Load the vertex shader, provide it with the source code and compile it.
	GLuint pvs = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(pvs, 1, &vertex_shader, NULL);
	glCompileShader(pvs);

	// Step 2:
	// Load the fragment shader, provide it with the source code and compile it.
	GLuint pfs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(pfs, 1, &fragment_shader, NULL);
	glCompileShader(pfs);

	// step 3:
	// Create the shader program
	m_postShader = glCreateProgram();

	// Step 4:
	// attach the vertex and fragment shaders to the m_shader program
	glAttachShader(m_postShader, pvs);
	glAttachShader(m_postShader, pfs);

	// Step 5:
	// describe the location of the shader inputs the link the program
	glLinkProgram(m_postShader);

	// step 6:
	// delete the vs and fs shaders
	glDeleteShader(pvs);
	glDeleteShader(pfs);
}

void PostProcessor::UpdateScreenSize(unsigned int a_width, unsigned int a_height)
{
	if (m_windowWidth != a_width)
	{
		m_windowWidth = a_width;
		m_windowHeight = a_height;
		SetupFrameBuffer();
	}
}

void PostProcessor::StartProcessing()
{
	glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
	glViewport(0, 0, m_windowWidth, m_windowHeight);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void PostProcessor::EndProcessing()
{
	// bind the back-buffer 
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, m_windowWidth, m_windowHeight);
	// just clear the back-buffer depth as 
	// each pixel will be filled 
	glClear(GL_DEPTH_BUFFER_BIT);
}
