#ifndef _SHADER_H_
#define _SHADER_H_
#include <string>

class Shader
{
public:
	Shader();
	Shader(const char* a_vertexFile);
	Shader(const char* a_vertexFile, const char* a_fragmentFile);
	Shader(const char* a_vertexFile, const char* a_fragmentFile, const char* a_geometryFile);
	~Shader();

	std::string LoadShader(const char* a_filename);

	//const char* GetVertex() { return m_vertex; }
	//const char* GetFragment() { return m_fragment; 

	std::string m_vertex;
	std::string m_fragment;
	std::string m_geometry;
};

#endif