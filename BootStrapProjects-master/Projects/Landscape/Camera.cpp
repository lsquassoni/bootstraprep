#include "Camera.h"

#include <glm\glm.hpp>
#include <glm\ext.hpp>
#include <glm\vec4.hpp>

using aie::Input;

Camera::Camera()
{

}

Camera::~Camera()
{

}

void Camera::Update(float deltaTime)
{
	/*Rotate camera based on mouse movement*/

	Input *input = Input::getInstance(); /*get a pointer to the input manager*/
	if (input->isMouseButtonDown(aie::INPUT_MOUSE_BUTTON_RIGHT))
	{
		float fRotationAmount = m_fMouseSensitivity * deltaTime;
		int iMouseX, iMouseY; /*Mouse X and Y coordinates from input*/
		float fXOffset, fYOffset; /*How fare we want to rotate the camera in these axes*/
		input->getMouseXY(&iMouseX, &iMouseY); /*Get mouse location and place it in our variables*/

		/*Find out how far the mouse has moved since last frame*/
		fXOffset = (iMouseX - m_iLastMouseXPos) * fRotationAmount;
		fYOffset = (iMouseY - m_iLastMouseYPos) * fRotationAmount;

		/* Use these values in our Yaw and Pitch values (so we can rotat that far)*/
		m_fYaw += fXOffset;
		m_fPitch += fYOffset;

		m_iLastMouseXPos = iMouseX;
		m_iLastMouseYPos = iMouseY;

		CameraInput(input, deltaTime);

		/*Calculate the new CameraLook*/
		CalculateLook();
	}
	else
	{
		input->getMouseXY(&m_iLastMouseXPos, &m_iLastMouseYPos);
	}
}

void Camera::LookAt(glm::vec3 target)
{
	glm::vec3 look = glm::normalize(target - m_vPosition); /* create a vector which is the direction to the target*/
	m_fPitch = glm::degrees(glm::asin(look.y));
	m_fYaw = glm::degrees(atan2(look.y, look.x));

	CalculateLook();
}

void Camera::SetPosition(glm::vec3 position)
{
	m_vPosition = position;
	CalculateView();
}

void Camera::CameraInput(Input *input, float deltaTime)
{
	input = Input::getInstance();
	if (input->isKeyDown(aie::INPUT_KEY_W))
	{
		m_vPosition += m_vCameraLook * m_fMoveSpeed * deltaTime;
	}
	if (input->isKeyDown(aie::INPUT_KEY_S))
	{
		m_vPosition -= m_vCameraLook * m_fMoveSpeed * deltaTime;
	}
	if (input->isKeyDown(aie::INPUT_KEY_A))
	{
		m_vPosition += glm::normalize(glm::cross(m_vCameraUp, m_vCameraLook)) * m_fMoveSpeed * deltaTime;
	}
	if (input->isKeyDown(aie::INPUT_KEY_D))
	{
		m_vPosition -= glm::normalize(glm::cross(m_vCameraUp, m_vCameraLook)) * m_fMoveSpeed * deltaTime;
	}
	if (input->isKeyDown(aie::INPUT_KEY_SPACE))
	{
		m_vPosition += m_vCameraUp * m_fMoveSpeed * deltaTime;
	}
	if (input->isKeyDown(aie::INPUT_KEY_LEFT_CONTROL))
	{
		m_vPosition -= m_vCameraUp * m_fMoveSpeed * deltaTime;
	}
}

void Camera::GetFrustumPlanes(const glm::mat4 & transform, glm::vec4 * planes)
{
	planes[0] = glm::vec4(transform[0][3] - transform[0][0],
						  transform[1][3] - transform[1][0],
						  transform[2][3] - transform[2][0],
						  transform[3][3] - transform[3][0]);
	
	planes[1] = glm::vec4(transform[0][3] + transform[0][0],
						  transform[1][3] + transform[1][0],
						  transform[2][3] + transform[2][0],
						  transform[3][3] + transform[3][0]);

	planes[2] = glm::vec4(transform[0][3] - transform[0][1],
						  transform[1][3] - transform[1][1],
						  transform[2][3] - transform[2][1],
						  transform[3][3] - transform[3][1]);
														 
	planes[3] = glm::vec4(transform[0][3] + transform[0][1],
						  transform[1][3] + transform[1][1],
						  transform[2][3] + transform[2][1],
						  transform[3][3] + transform[3][1]);

	planes[4] = glm::vec4(transform[0][3] - transform[0][2],
						  transform[1][3] - transform[1][2],
						  transform[2][3] - transform[2][2],
						  transform[3][3] - transform[3][2]);
														 
	planes[5] = glm::vec4(transform[0][3] + transform[0][2],
						  transform[1][3] + transform[1][2],
						  transform[2][3] + transform[2][2],
						  transform[3][3] + transform[3][2]);

	for (int i = 0; i < 6; i++)
	{
		float d = glm::length(vec3(planes[i]));
		planes[i] /= d;
	}
}

void Camera::CalculateLook()
{
	/*Some circle geometry maths to convert the viewing angle from
	Yaw, Pitch and Roll in to a single normalized vector*/
	if (m_fPitch >= 90)
		m_fPitch = 89.9;
	if (m_fPitch <= -90)
		m_fPitch = -89.9;

	vec3 look;
	look.x = glm::cos(glm::radians(m_fYaw)) * glm::cos(glm::radians(m_fPitch));
	look.y = glm::sin(glm::radians(m_fPitch)) * glm::cos(glm::radians(m_fRoll));
	look.z = glm::sin(glm::radians(m_fYaw)) * glm::cos(glm::radians(m_fPitch));
	m_vCameraLook = glm::normalize(look);

	CalculateView();
}

void Camera::CalculateView()
{
	m_mViewMatrix = glm::lookAt(m_vPosition, m_vPosition + m_vCameraLook, m_vCameraUp);
}

const glm::mat4 & Camera::GetView()
{
	return m_mViewMatrix;
}
