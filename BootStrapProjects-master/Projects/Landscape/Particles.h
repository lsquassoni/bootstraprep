#ifndef _PARTICLES_H_
#define _PARTICLES_H_

#include <glm\mat4x4.hpp>
#include <glm\vec2.hpp>
#include <glm\vec3.hpp>
#include <Texture.h>
#include <vector>


class Camera;

using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat4;

struct ParticleVertex
{
	vec4 position;
	vec4 colour;
	vec2 texCoord;
};

struct Particle
{
	 vec3	m_position;
	 vec3	m_velocity;
	 vec4	m_color;
	float	m_size;
	float	m_lifespan;
	float	m_lifetime;
};

class ParticleEmitter
{
public:
	ParticleEmitter();
	virtual ~ParticleEmitter();

	void initialize(unsigned int a_maxParticles, unsigned int a_emitRate,
		float a_lifetimeMin, float a_lifetimeMax, float a_velocityMin,
		float a_velocityMax, float a_startSize, float a_endSize,
		const vec4& a_startColor, const vec4& a_endColor, vec3& a_direction = vec3(0,0,0));

	void SetTexture(aie::Texture* a_texture);

	void emit();

	void update(float a_deltaTime, Camera* a_camera, vec3 a_emitterPosition);

	void draw(mat4 a_projection);

	void loadShader(const char* a_vs, const char* a_fs);

	unsigned int GetMaxParticles() { return m_maxParticles; }

	unsigned int	m_particleShader;
	std::vector<Particle*> m_currentParticles;
protected:
	aie::Texture*	m_texture;

	Particle*		m_particles;			//Array of particle objects
	unsigned int	m_firstDead;			//The index of the first dead particle
	unsigned int	m_maxParticles;			//The maximum amount of particles

	unsigned int	m_vao, m_vbo, m_ibo;	//The vertex array, vertex buffer and index buffer
	ParticleVertex* m_vertexData;			//Array of particle vertices

	vec3			m_position;				//Position of particle emitter.

	float			m_emitTimer;			//The timer at which particles are emitted
	float			m_emitRate;				//The rate at which particles are emitted.

	float			m_lifespanMin;			//Minimum lifespan of a particle
	float			m_lifespanMax;			//Maximum lifespan of a particle

	float			m_velocityMin;			//Minimum velocity of a particle
	float			m_velocityMax;			//Maximum velocity of a particle

	float			m_startSize;			//The size of a particle at emmission
	float			m_endSize;				//The size of a particle at it's death

	vec4			m_startColor;			//The color of a particle at emmission
	vec4			m_endColor;				//The color of a particle at death

	vec3			m_direction;

};

class ExplosionEmitter : public ParticleEmitter
{
public:
	void emit();

	void update(float a_deltaTime, Camera* a_camera, vec3 a_emitterParticle);
};

struct GPUParticle
{
	GPUParticle() : lifetime(1), lifespan(0) {}

	vec3 position;
	vec3 velocity;
	float lifetime;
	float lifespan;
};

class GPUParticleEmitter
{
public:
	GPUParticleEmitter();
	virtual ~GPUParticleEmitter();

	void Initialise(unsigned int a_maxParticles,
		float a_lifespanMin, float a_lifespanMax,
		float a_velocityMin, float a_velocityMax,
		float a_startSize, float a_endSize,
		const vec4& a_startColor,
		const vec4& a_endColor);

	void Draw(float time, const mat4& a_cameraTransform, const mat4& a_projectionView);

protected:
	void CreateBuffers();
	void CreateUpdateShader();
	void CreateDrawShader();

	GPUParticle* m_particles;

	unsigned int m_maxParticles;

	vec3 m_position;

	float m_lifespanMin;
	float m_lifespanMax;

	float m_velocityMin;
	float m_velocityMax;

	float m_startSize;
	float m_endSize;

	vec4 m_startColor;
	vec4 m_endColor;

	unsigned int m_activeBuffer;

	unsigned int m_vao[2];
	unsigned int m_vbo[2];

	unsigned int m_drawShader;
	unsigned int m_updateShader;

	float m_lastDrawTime;
};


#endif
