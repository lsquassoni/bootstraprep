#include "Shader.h"
#include <iostream>
#include <fstream>

Shader::Shader()
{
}

Shader::Shader(const char * a_vertexFile)
{
	m_vertex = LoadShader(a_vertexFile);
	m_fragment = "";
	m_geometry = "";
}

Shader::Shader(const char * a_vertexFile, const char * a_fragmentFile)
{
	m_vertex = LoadShader(a_vertexFile);
	m_fragment = LoadShader(a_fragmentFile);
	m_geometry = "";
}

Shader::Shader(const char * a_vertexFile, const char * a_fragmentFile, const char * a_geometryFile)
{
	m_vertex = LoadShader(a_vertexFile);
	m_fragment = LoadShader(a_fragmentFile);
	m_geometry = LoadShader(a_geometryFile);
}

Shader::~Shader()
{
}

std::string Shader::LoadShader(const char* a_filename)
{
	std::ifstream readFile;
	readFile.open(a_filename);
	std::string temp;
	std::string output;
	if (readFile.is_open())
	{
		while (!readFile.eof())
		{
			std::getline(readFile, temp);
			output += temp;
			output += "\n";
		}
	}
	readFile.close();
	return output;
}
