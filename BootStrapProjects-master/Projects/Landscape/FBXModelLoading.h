#ifndef _FBXMODELLOADING_H_
#define _FBXMODELLOADING_H_

#include "../FBXLoader/FBXFile.h"

class FBXLoader
{
public:
	FBXLoader();
	~FBXLoader();

	void CreateFBXOpenGLBuffers(FBXFile *a_file);
	void CleanupFBXOpenGLBuffers(FBXFile *a_file);

	void LoadShaders(const char* a_vS, const char* a_fS);
	void UnloadShaders();

	void Update(float deltaTime);

	void draw(glm::mat4 a_ProjectView, glm::vec3 a_lightPos, glm::vec3 a_lightColor, glm::vec3 a_specColor, glm::vec3 a_cameraPos, float a_lightAmb, float a_lightStrength);

	void SetShader(unsigned int *a_Shader);
	void SetVAO(unsigned int a_VAO);
	void SetVBO(unsigned int a_VBO);
	void SetIBO(unsigned int a_IBO);

	void RotateModel(glm::mat4* a_modelMat, float a_angle);

	unsigned int GetShader() { return m_shader; };
	unsigned int GetVAO() { return vao; };
	unsigned int GetVBO() { return vbo; };
	unsigned int GetIBO() { return ibo; };
	glm::mat4 GetModel() { return model; };

	FBXFile* m_myFbxModel;
	float m_fbxAnimationTime = 0;

private:
	unsigned int m_shader;
	
	unsigned int vao;
	unsigned int vbo;
	unsigned int ibo;

	glm::mat4 model;
};
#endif
